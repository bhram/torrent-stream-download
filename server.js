var express = require("express");
var app = express();
var prettierBytes = require('prettier-bytes');
var server = require("http").Server(app);
var io = require("socket.io")(server);
var WebTorrent = require("webtorrent");
var moment = require('moment');
var path = require("path");
var fs = require("fs");
var del = require('del');
var mime = require("mime");


const PORT = process.env.PORT || 5000;
const DIR = path.join(__dirname, 'downloader');;

var client = new WebTorrent();
var torrent;
app.use(express.static(__dirname));

app.get("/torrent/:filename", (req, res) => {
    var fpath = path.join(DIR, req.params.filename);
    fs.stat(fpath, function (err, stat) {
        if (err == null) {
            res.sendfile(fpath)
        } else {
            res.end("<h1>404 - Not Found !</h1>");
        }
    })
});

io.on('connection', function (socket) {
    if (torrent && torrent.files.length) sendTorrent();
    socket.on("add-torrent", addTorrent);
    socket.on("remove-torrent", removeTorrent);

});


server.listen(PORT, () => {
    console.log("Start Working in Port : " + PORT);
})

function removeTorrent(magnet) {
    client.remove(magnet);
}

client.on("error", function (msg) {
    console.log(msg.message)
})

var intervalProgress;

function addTorrent(magnet) {
    torrent = client.add(magnet, { path: DIR });
    torrent.on("ready", function () {
        sendTorrent();
        torrent.on('done', onDone)
        intervalProgress = setInterval(Progressing, 500)
    });
}

function sendTorrent() {
    var req = {
        files: torrent.files.map((file) =>
        { return { name: file.name, path: file.path, length: file.length } }).reverse(),
        title: torrent.name,
    };
    io.emit("success-torrent", req)
}

function Progressing() {
    var req = { info: {} };
    req.info.speed = prettierBytes(torrent.downloadSpeed)
    req.info.downloaded = prettierBytes(torrent.downloaded) + "   OF   " + prettierBytes(torrent.length)
    req.info.remaining = moment.duration(torrent.timeRemaining / 1000, 'seconds').humanize()
    req.progress = Math.floor(torrent.progress * 100);
    io.emit("progress", req)
}

function onDone() {
    clearInterval(intervalProgress);
    io.emit("done")
    console.log("Completed");
}
