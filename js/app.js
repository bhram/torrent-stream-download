
var app = angular.module("app", []).controller("main", function ($scope) {

$scope.magnet = "magnet:?xt=urn:btih:3e2f66d23507758145b590610924ac8d5c83e7cd&dn=The.Wall.2017.WEB-DL.x264-FGT&tr=http%3A%2F%2Ftracker.trackerfix.com%3A80%2Fannounce&tr=udp%3A%2F%2F9.rarbg.me%3A2710&tr=udp%3A%2F%2F9.rarbg.to%3A2710";
//   $scope.magnet="magnet:?xt=urn:btih:fb93f89c6c2aff6545c8e19f8cf16f473d803bbd&dn=Everything.Everything.2017.1080p.BluRay.H264.AAC-RARBG&tr=http%3A%2F%2Ftracker.trackerfix.com%3A80%2Fannounce&tr=udp%3A%2F%2F9.rarbg.me%3A2710&tr=udp%3A%2F%2F9.rarbg.to%3A2710";
    var socket = io();

    $scope.size = byteSize;
    $scope.encode = encodeURIComponent;
    $scope.isAdded = function(){
        return $scope.loading || $scope.torrent;
    }
    
    $scope.handlerTorrent = function () {
        socket.emit(!$scope.isAdded() ? "add-torrent" : "remove-torrent", $scope.magnet);
          $scope.loading = !$scope.loading;
    };

    socket.on("success-torrent", function (torrent) {
        $scope.loading = false;
        $scope.torrent = torrent;
        $scope.$apply();
    });

    socket.on("error-torrent", function (message) {
        $scope.loading = false;

    });

    socket.on("progress",function(progress){
        $scope.progress = progress;
        $scope.$apply();
    })

    socket.on("done",function(){
        $scope.progress = null;
        $scope.$apply();
        toastr.success('Torrent is Completed !')
    })
	
});

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

function byteSize(bytes, precision) {
    var kilobyte = 1024,
        megabyte = kilobyte * 1024,
        gigabyte = megabyte * 1024,
        terabyte = gigabyte * 1024;

    if ((bytes >= 0) && (bytes < kilobyte)) {
        return bytes + " B";

    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
        return (bytes / kilobyte).toFixed(precision) + " KB";

    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
        return (bytes / megabyte).toFixed(precision) + " MB";

    } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
        return (bytes / gigabyte).toFixed(precision) + " GB";

    } else if (bytes >= terabyte) {
        return (bytes / terabyte).toFixed(precision) + " TB";

    } else {
        return bytes + " B";
    }
}